package com.aut.tjb0436.bikem8v11.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aut.tjb0436.bikem8v11.Models.ColorScheme;
import com.aut.tjb0436.bikem8v11.Models.MapsFormat;
import com.aut.tjb0436.bikem8v11.Models.Speed;
import com.aut.tjb0436.bikem8v11.Preferences.PrefsActivity;
import com.aut.tjb0436.bikem8v11.R;
import com.aut.tjb0436.bikem8v11.Utilities.BluetoothSingleton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.TimeZone;


/**
 * Created by Terry on 26/01/16.
 */
public class MapsFragment extends SupportMapFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected static final String TAG = "MapsFragment";     //LogCat message TAG
    private ColorScheme mColorScheme;
    private MapsFormat mMapsFormat;
    private int mColor;
    //colour values sent to arduino
    private final int LIGHT_BLUE = -11608579;
    private final int GREEN = -12779776;
    private final int ORANGE = -48128;

    //values for bitmap manipulation
    private final int CROPPED_WIDTH = 150;
    private final int CROPPED_HEIGHT = 128;
    private final int COLUMN_COUNT = 5;
    private final int ROW_COUNT = 128;
    private final int PIXEL_BLOCK_LENGTH = 1;
    private final int PIXEL_BLOCK_HEIGHT = 1;
    private int mWidth, mHeight;
    private Bitmap bitmap;

    private GoogleMap mMap;
    private boolean buildings;
    private View view, appView;
    private static final String BT_SINGLETON = "btSingleton";

    /**
     * Bluetooth
     */
    private BluetoothSingleton mBluetoothSingleton;
    private BluetoothSocket btSocket;
    private OutputStream mOutputStream;

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Location Services elements
     */
    protected Location mLastLocation;
    protected Location mCurrentLocation;
    protected LocationRequest mLocationRequest;
    protected boolean mRequestingLocationUpdates = false;

    /**
     * Speed fields
     */
    private Speed mSpeed;
    private int speed = 0;

    /**
     * Text elements
     */
    protected TextView mTimeDate;
    protected ImageView bitmapImage; //need to remove imageView from layout file
// TODO: 8/02/16 add view for overlaid speed

    /**********************************************************************
     *********************** Lifecycle Methods ****************************
     **********************************************************************/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBluetoothSingleton = BluetoothSingleton.getInstance();

//        mBluetoothSingleton = (BluetoothSingleton) getArguments().getSerializable(BT_SINGLETON);
////        Log.d(TAG, mBluetoothSingleton.TestString());
////        mBluetoothSingleton.createBTSocket();
//        btSocket = mBluetoothSingleton.getBtSocket();
        mBluetoothSingleton.createOutputStream();
        mOutputStream = mBluetoothSingleton.getOutputStream();
//        if(!btSocket.isConnected()) {
//            try {
//                btSocket.connect();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            try {
//                mOutputStream = btSocket.getOutputStream();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        else Log.d(TAG, "Bollox");

        mSpeed = new Speed(getActivity());              //Speed object used to get speed and speed units preferences
        mColorScheme = new ColorScheme(getActivity());
        mMapsFormat = new MapsFormat(getActivity());

        /**
         * Start Location Services requests
         */
        buildGoogleApiClient();                 //Builds the GoogleAPIClient below
        createLocationRequest();                //Sets LocationRequest intervals and others
        mRequestingLocationUpdates = true;      //Requests will begin with this being true
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        view = inflater.inflate(R.layout.fragment_maps, container, false);

//        appView = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
        appView = getActivity().getWindow().getDecorView();

        ConfigureViews();

        return view;
    }

    /**
     * onStart called when activity becomes visible to the user
     */
    @Override
    public void onStart() {
        super.onStart();
//        mGoogleApiClient.registerConnectionCallbacks(this);
        mGoogleApiClient.connect();
    }

    /**
     * onResume called when activity will start interacting with the user
     */
    @Override
    public void onResume() {
        super.onResume();
        FragmentManager fragmentManager = getChildFragmentManager();
        SupportMapFragment mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
            }
        });
        mGoogleApiClient.registerConnectionCallbacks(this);
        mColorScheme.setColor();
        mColor = mColorScheme.getColor();
        mTimeDate.setTextColor(mColor);
    }

    /**
     * onPause called when user is leaving the fragment
     */@Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
        mGoogleApiClient.unregisterConnectionCallbacks(this);
        Log.d(TAG, "Updates stopped");
    }

    /**
     * Fragment will be stopped
     */
    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        mGoogleApiClient.unregisterConnectionCallbacks(this);
    }

    /**
     * Called after onDestroyView() to do final clean up of fragment state
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.unregisterConnectionCallbacks(this);
    }

    /*********************************************************************
     *******************Google Location Services**************************
     *********************************************************************/

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Sets LocationRequest variables
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000); //need to change these to 3000 once baud is at 115200
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mCurrentLocation == null) {
            Toast.makeText(getActivity(), R.string.no_location_detected, Toast.LENGTH_LONG).show();
        }

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }

         mMap.setMyLocationEnabled(true);
    }

    /**
     * Get the location updates using parameters set in createLocationRequest()
     */
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = mCurrentLocation;
        mCurrentLocation = location;
        updateUI();
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }
    @Override
    public void onConnectionSuspended(int i) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    /**********************************************************************
     ***************************Utilities**********************************
     **********************************************************************/

    private void ConfigureViews() {
        mColorScheme.setColor();
        mColor = mColorScheme.getColor();
        mMapsFormat.setThreeDBuildings();
        mSpeed.setSpeedUnits();

        /**
         * Set up TextView to display current time and date
         */
        mTimeDate = (TextView) view.findViewById(R.id.time_date);
        Typeface expansiva = Typeface.createFromAsset(getActivity().getAssets(), "fonts/expansiva_bold.ttf");
        mTimeDate.setTypeface(expansiva);
        mTimeDate.setTextColor(mColor);

        bitmapImage = (ImageView) view.findViewById((R.id.bitmap));

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        FragmentManager fragmentManager = getChildFragmentManager();
//        SupportMapFragment mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
//        mapFragment.getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(GoogleMap googleMap) {
//                mMap = googleMap;
//            }
//        });
    }

    private void updateUI() {
        LatLng mMyLocation = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mMyLocation, 18));
//        animateMarker(mMap.addMarker(new MarkerOptions()), mMyLocation, false);
        buildings = mMapsFormat.getThreeDBuildings();
        mMap.setBuildingsEnabled(buildings);
        Log.d(TAG, "buildings2 = " + buildings);

        //get view of BikeM8 app only

        //take snapshot, reduce to 50% and crop to 160 x 128 pixels
        takeSnapShot();


        //put bitmap image in cache memory
//        LruCache cachedImage = cacheImage(map);

        speed = (int) mSpeed.getSpeed(mCurrentLocation, mLastLocation);

        int currentColour = currentColor();
        String dateTime = getTimeDate();
        String speedString = Integer.toString(speed);
        int units = getSpeedUnits(mSpeed.getSpeedUnits());

//        if(isVisibleToUser()) {

//        String data = "h" + currentColour + "*";
//        sendData(data);
//        try {
//            Thread.sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        data = "d" + dateTime + "*";
//        sendData(data);
//        try {
//            Thread.sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        data = "s" + speedString + "*";
//        sendData(data);
//        try {
//            Thread.sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        data = "k" + units + "*";
//        sendData(data);
//        try {
//            Thread.sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        //slice bitmap into pieces of 64 bytes to send to Arduino so that its
        //serial buffer does not overflow
        if(bitmap != null) {
            for (int i = 0; i < 1; i++) {
                for (int j = 0; j < 1; j++) {
                    Bitmap slice = Bitmap.createBitmap(bitmap, 0, 0, PIXEL_BLOCK_LENGTH * (i + 1), PIXEL_BLOCK_HEIGHT * (j + 1));
                    int bytes = slice.getByteCount();
                    Log.d(TAG, "bitmap slice byte count = " + bytes);
                    sendImage(slice, i, j);
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


//    private LruCache cacheImage(Bitmap map) {
//        //create cache memory based on amount available to app and required size to hold bitmap
//        int memClass = ((ActivityManager) getActivity().getSystemService(getContext().ACTIVITY_SERVICE)).getMemoryClass();
//        Log.d(TAG, "memory available for the app is " + memClass);
//        int cacheSize = memClass;
//        LruCache cache = new LruCache<>(cacheSize);
//
//        cache.put("bitmap", map);
//        return cache;
//    }

    //takes snapshot of current map image and processes it to be a suitable size to send to Arduino
    private void takeSnapShot() {

        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bm;
            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                //take snapshot of current map
                bm = snapshot;
                int newWidth = appView.getWidth()/2;
                int newHeight = appView.getHeight()/2;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bm, newWidth, newHeight, true);

                bitmap = Bitmap.createBitmap(scaledBitmap, (scaledBitmap.getWidth() - CROPPED_WIDTH)/2,
                        (scaledBitmap.getHeight() - CROPPED_HEIGHT)/2, CROPPED_WIDTH, CROPPED_HEIGHT);
                //convert 32 bit bitmap to 16 bit
                bitmap = convert(bitmap, Bitmap.Config.RGB_565);
                bitmapImage.setImageBitmap(bitmap); //need to remove imageView from layout file
                int bytes = bitmap.getByteCount();
                Log.d(TAG, "bitmap byte count = " + bytes);
                Log.d(TAG, "bitmap width = " + (bitmap.getWidth()));
                Log.d(TAG, "bitmap height = " + (bitmap.getHeight()));
            }
        };
        mMap.snapshot(callback);
    }

    //converts a 32 bit bitmap to 16 bit RGB_565
    private Bitmap convert(Bitmap bitmap, Bitmap.Config config) {
        Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
        Canvas canvas = new Canvas(convertedBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return convertedBitmap;
    }

    //gets bitmap ready to send to Arduino
//    private Bitmap configureSnapshot() {
//        takeSnapShot();
//        return bitmap;
//    }

    private void sendImage(Bitmap imageSlice, int i, int j) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(60);
        imageSlice.compress(Bitmap.CompressFormat.PNG, 0, baos);
        byte[] baosArray = baos.toByteArray();
        Log.d(TAG, "baosArray.length = " + baosArray.length);
        String startID = "m";
        String testID = "Hello";
        String endID;
        byte[] start = startID.getBytes(Charset.forName("UTF-8"));
        byte[] test = testID.getBytes(Charset.forName("UTF-8"));
//        if(i == COLUMN_COUNT - 1 && j == ROW_COUNT - 1) endID = "*";
        if(i == 0 && j == 0) endID = "*";
        else endID = "q";
        byte[] end = endID.getBytes(Charset.forName("UTF-8"));
        byte[] mapBytes = baos.toByteArray();
        try {
            mOutputStream.write(start); //1 byte
//            mOutputStream.write(test);
            mOutputStream.write(mapBytes); //60 bytes
            mOutputStream.write(end); //1 bytes
            mOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "mapBytes.length = " + mapBytes.length);
    }

    /**
     * Sets the current time and date and displays it at the top of the activity
     */
    public String getTimeDate() {
        long date = System.currentTimeMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
        TimeZone.getDefault();
        String dateString = sdf.format(date);
        mTimeDate.setText(dateString);
        return dateString;
    }

    public int currentColor() {
        int colour = 0;
        switch(mColor) {
            case LIGHT_BLUE: colour = 1;
                break;
            case GREEN: colour = 2;
                break;
            case ORANGE: colour = 3;
        }
        return colour;
    }

    public int getSpeedUnits(String speedUnits) {
        int units = 0;
        switch(speedUnits) {
            case "km/h": units = 1;
                break;
            case "mph": units = 2;
        }
        return units;
    }

    // Method to send data
    private void sendData(String message) {
        byte[] msgBuffer = message.getBytes();
        try {
            //attempt to place data on the outstream to the BT device
            mOutputStream.write(msgBuffer);
            mOutputStream.flush();
            Log.d(TAG, "bytes at index 1 are " + msgBuffer[0]);
        } catch (IOException e) {
            //if the sending fails this is most likely because device is no longer there
            /**
             * start bikem8 and connect to bluetooth
             */
//            mDeviceNotFound.setVisibility(View.VISIBLE);
//            mDeviceNotFound.setText(R.string.turn_on_device);
//            mContinueButton.setVisibility(View.VISIBLE);
//            mContinueButton.setEnabled(true);
//            Toast.makeText(getBaseContext(), "ERROR - Device not found", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "sendData");
//            finish();
        }
    }

    /**************************************************************************
     *****************************Menu Options*********************************
     **************************************************************************/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        menu.clear();
        inflater.inflate(R.menu.menu_splash, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getActivity(), PrefsActivity.class);
            startActivity(intent);              //Starts PrefsActivity from ActionBar or Menu button

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;
        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }
}
