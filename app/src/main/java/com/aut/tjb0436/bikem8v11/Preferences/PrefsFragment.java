package com.aut.tjb0436.bikem8v11.Preferences;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.aut.tjb0436.bikem8v11.R;

public class PrefsFragment extends PreferenceFragment {

    protected static final String TAG = "PrefsFragment";     //LogCat message TAG
    private OnSharedPreferenceChangeListener mPreferenceChangeListener;
    private SharedPreferences prefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: 9/02/16 include saved instance state for preferences to show their last chosen state

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

//        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mPreferenceChangeListener = new OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key){
                Log.d(TAG, "pref key string = " + sharedPreferences.getString(key, ""));
                switch(key) {
                    case "pref_speed_units":
                        setSharedPrefSummary(sharedPreferences, key);
                        break;
                    case "pref_maps_display_format":
                        setSharedPrefSummary(sharedPreferences, key);
                        break;
                    case "pref_color_scheme":
                        setSharedPrefSummary(sharedPreferences, key);
                        break;
                }
            }
        };
        prefs.registerOnSharedPreferenceChangeListener(mPreferenceChangeListener);
    }

    public void setSharedPrefSummary(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        pref.setSummary(sharedPreferences.getString(key, ""));
    }

    @Override
    public void onResume() {
        super.onResume();
        prefs.registerOnSharedPreferenceChangeListener(mPreferenceChangeListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        prefs.unregisterOnSharedPreferenceChangeListener(mPreferenceChangeListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        prefs.unregisterOnSharedPreferenceChangeListener(mPreferenceChangeListener);
    }
}
