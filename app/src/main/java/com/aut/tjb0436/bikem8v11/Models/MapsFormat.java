package com.aut.tjb0436.bikem8v11.Models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by Terry on 8/02/16.
 */
public class MapsFormat {

    protected static final String TAG = "MapsFormat";     //LogCat message TAG
    public static final String KEY_PREF_MAPS_DISPLAY_FORMAT = "pref_maps_display_format";
    private boolean buildings;
    private Context mContext;

    public MapsFormat(Context context) {
        mContext = context;
        buildings = false;
    }

    public void setThreeDBuildings() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        String result = sharedPref.getString(KEY_PREF_MAPS_DISPLAY_FORMAT, "2D");
        if(result.equals("2D")) buildings = false;
        else if(result.equals("3D")) buildings = true;
        Log.d(TAG, "buildings = " + buildings);
    }

    public boolean getThreeDBuildings() {
        return buildings;
    }
}
