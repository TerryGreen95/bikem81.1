package com.aut.tjb0436.bikem8v11.Models;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;

import com.aut.tjb0436.bikem8v11.R;

import static android.provider.Settings.System.getString;

/**
 * Created by Terry on 9/12/15.
 */
public class Speed {
    private Context mContext;
    public static final String KEY_PREF_SPEED_UNITS = "pref_speed_units";
    private double mSpeed;
    private String mSpeedUnits;
    private boolean isKmh;
    private static final String TAG = "Speed Class";
    private static final double METRES_SEC_TO_KMH = 3.6;
    private static final double KMH_TO_MPH = 0.621371;
    private static final int METRES_TO_KM = 1000;
    private static final int MILLIS_TO_HOURS = 3600000;

    public Speed(Context context) {
        mContext = context;
        mSpeed = 0.0;
        mSpeedUnits = "km/h";
        isKmh = true;
    }

    /**
     * Calculate speed from GPS calls or if !hasSpeed()
     * use distance and time between last 2 locations to calculate speed
     */
    public double getSpeed(Location mCurrentLocation, Location mLastLocation) {
        Log.d(TAG, "Speed Units = " + getSpeedUnits());
        if(mCurrentLocation.hasSpeed()) {
            mSpeed = mCurrentLocation.getSpeed() * METRES_SEC_TO_KMH;               //getSpeed() returns m/s so multiply by 3.6 to km/h
            if(!isKmh())
                mSpeed *= KMH_TO_MPH;
            Log.d(TAG, "mSpeed = " + mSpeed);
        }
        else {
            double elapsedTime = (double)((mCurrentLocation.getTime() - mLastLocation.getTime()) / MILLIS_TO_HOURS);      //getTime returns time in millis since 1.1.70 so divide by 3.6 x 10^6 to get hours
            float distance = mLastLocation.distanceTo(mCurrentLocation) / METRES_TO_KM;                                   //distanceTo is returned in metres so divide by 1000 to get km
            mSpeed = distance/elapsedTime;
            if(Double.isNaN(mSpeed) || mSpeed > METRES_TO_KM) {
                mSpeed = 0.0;
            }
            if(!isKmh())
                mSpeed *= KMH_TO_MPH;
        }
        return mSpeed;
    }

    public String getSpeedUnits() {
        return mSpeedUnits;
    }

    public void setSpeedUnits() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
        String speedUnitsPref = sharedPref.getString(KEY_PREF_SPEED_UNITS, "km/h_default");
        mSpeedUnits = speedUnitsPref;
        if(mSpeedUnits.equals("km/h (Default)")) {
            mSpeedUnits = "km/h";
        }
    }

    public boolean isKmh() {
        if(mSpeedUnits.equals("km/h"))
            isKmh = mSpeedUnits.equals("km/h");
        else
            isKmh = false;
        Log.d(TAG, "isKmh = " + isKmh);
        return isKmh;
    }
}
