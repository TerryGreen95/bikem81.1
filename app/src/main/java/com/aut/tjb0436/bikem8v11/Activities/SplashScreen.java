package com.aut.tjb0436.bikem8v11.Activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aut.tjb0436.bikem8v11.R;
import com.aut.tjb0436.bikem8v11.Preferences.PrefsActivity;
import com.aut.tjb0436.bikem8v11.Utilities.BluetoothSingleton;
import com.aut.tjb0436.bikem8v11.Utilities.ViewPagerActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Set;

public class SplashScreen extends AppCompatActivity  {

    protected static final String TAG = "SplashScreen";     //LogCat message TAG
    private static int SPLASH_SCREEN_TIME = 3000;   //delay of 3 seconds

    /**
     * TextView and Button fields
     */
    private TextView mLogo;
    private TextView mDeviceNotFound;
    private TextView mUnpairedListTitle;
    private ListView mUnpairedList;
    private TextView mConnectionStatus;
    private ProgressDialog mProgressDialog;
    private Button mRetryButton;
    private Button mContinueButton;

    /**
     * Bluetooth
     */
    private BluetoothSingleton mBluetoothSingleton;
    private BluetoothAdapter mBluetoothAdapter;
    private final int REQUEST_ENABLE_BT = 1;
    private String mMacAddress = "00:00:00:00:00:00";
    private final int MAC_ADDRESS_LENGTH = 17;
    private Set<BluetoothDevice> mPairedSet;
    private ArrayList<BluetoothDevice> mPairedDevices, mUnpairedDevices;
    private ArrayAdapter<String> mUnpairedArrayAdapter;
    private boolean mIsPaired, mIsFound, mIsDeviceOn, mBluetoothEnabled, mConnectionTried, mHasRun;
    private BluetoothSocket btSocket = null;

    /**
     * set up buttons and textfields to be displayed on SplashScreen
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mBluetoothSingleton = BluetoothSingleton.getInstance();

        /**
         * The mLogo will be an imageView eventually
         */
        mLogo = (TextView) findViewById(R.id.app_name);
        Typeface expansiva = Typeface.createFromAsset(this.getAssets(), "fonts/expansiva_bold.ttf");
        mLogo.setTypeface(expansiva);
        mLogo.setTextSize(108);
        mUnpairedListTitle = (TextView) findViewById(R.id.title_unpaired_devices);
        mUnpairedListTitle.setVisibility(View.GONE);
        mDeviceNotFound = (TextView) findViewById(R.id.device_not_found);
        mDeviceNotFound.setVisibility(View.GONE);
        mDeviceNotFound.setGravity(Gravity.CENTER);
        mConnectionStatus = (TextView) findViewById(R.id.connecting);
        mConnectionStatus.setTextSize(40);

        /**
         * Set up buttons
         */
        mRetryButton = (Button) findViewById(R.id.retry_button);
        mRetryButton.setVisibility(View.GONE);
        mRetryButton.setEnabled(false);
        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Check 2");
                mDeviceNotFound.setVisibility(View.GONE);
                mRetryButton.setVisibility(View.GONE);
                mRetryButton.setEnabled(false);
                mBluetoothAdapter.startDiscovery();
            }
        });

        mContinueButton = (Button) findViewById(R.id.continue_button);
        mContinueButton.setVisibility(View.GONE);
        mContinueButton.setEnabled(false);
        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Check 2");
                mDeviceNotFound.setVisibility(View.GONE);
                mContinueButton.setVisibility(View.GONE);
                mContinueButton.setEnabled(false);
              connectDevices();
            }
        });

        //gets default values from PreferenceManager when run for the first time
        PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.preferences, false);

        //data structures to handle list of bluetooth devices
        mPairedDevices = new ArrayList<>();
        mUnpairedDevices = new ArrayList<>();
        mUnpairedArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_expandable_list_item_1);
        mUnpairedList = (ListView) findViewById(R.id.unpaired_devices);
        mUnpairedList.setAdapter(mUnpairedArrayAdapter);

        /**
         * Set initial boolean values
         */
        mIsPaired = false;
        mIsFound = false;
        mIsDeviceOn = false;
        mBluetoothEnabled = false;
        mConnectionTried = false;
        mHasRun = false;

        /**
         * Check for Bluetooth capability
         */
        mBluetoothAdapter = mBluetoothSingleton.getBluetoothAdapter();
        if (mBluetoothAdapter == null) {
            // TODO: 16/02/16 add textview to tell user they have no bluetooth functionality
            Log.d(TAG, "No Bluetooth Present");
        }

        /**
         * Turn on Bluetooth if it isn't already
         */
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            Log.d(TAG, "Activity Returned");
        }
        else mBluetoothEnabled = true;

        // Register the Bluetooth BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy

        //start looking for bluetooth devices in new thread after 3 seconds of SplashScreen showing
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                queryDevices();

//                if(!mIsFound || !mIsPaired || !mIsDeviceOn) queryDevices();
//                else {
//                    connectDevices();
//                    Log.d(TAG, "connect devices called from Runnable");
//                }
        }
    }, SPLASH_SCREEN_TIME);
}

    /**
     * Querying paired devices
     */
    public void queryDevices() {
//        Log.d(TAG, "mIsPaired = " + mIsPaired);
//        Log.d(TAG, "mIsFound = " + mIsFound);
//        Log.d(TAG, "mIsDeviceOn = " + mIsDeviceOn);
//        Log.d(TAG, "mConnectionTried = " + mConnectionTried);
//        Log.d(TAG, "mHasRun = " + mHasRun);
        mHasRun = true; //deals with case when app has been paused and then resumed
        mPairedDevices.clear();
        mPairedSet = mBluetoothAdapter.getBondedDevices();

        /**
         * Loop through paired devices. Check if BT module is paired
         */
        for (BluetoothDevice device : mPairedSet) {
            mPairedDevices.add(device);
            if (device.getName().equals("BikeM8")) {
                Log.d(TAG, "Check 4.1");
                mBluetoothSingleton.setMacAddress(device.getAddress()); //get mac address of bluetooth bee
                mMacAddress = mBluetoothSingleton.getMacAddress();
                mIsPaired = true;
            }
        }
        // If the device is found or paired already
        if (mIsFound || mIsPaired) {
            Log.d(TAG, "Check 3");
            // get the
//            for (BluetoothDevice newDevice : mUnpairedDevices) {
//                if (newDevice.getName().equals("BikeM8")) {  //nothing is happening in here why?
//                    Log.d(TAG, "Check 4.2");
//                }
//            }
            if(mIsPaired){
                Log.d(TAG, "Check 6");
                if(!mConnectionTried) connectDevices();
                else if(!mIsDeviceOn) {
                    /**
                     * start bikem8 and connect to bluetooth
                     */
                    mDeviceNotFound.setVisibility(View.VISIBLE);
                    mDeviceNotFound.setText(R.string.turn_on_device);
                    mContinueButton.setVisibility(View.VISIBLE);
                    mContinueButton.setEnabled(true);
                }
                else startBikeM8();
            }
            else {
                Log.d(TAG, "Check 5");
                mUnpairedListTitle.setVisibility(View.VISIBLE);
                mUnpairedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        BluetoothDevice unpairedItem = mUnpairedDevices.get(position); //sets unpaired BT module that was pressed as unpairedItem
                        if(unpairedItem.getBondState() != BluetoothDevice.BOND_BONDED) {
                            reloadListView();  //remove unpairedItem from unpaired list view
                            Log.d(TAG, "Pairing Text");
                            // Get the device MAC address, which is the last 17 chars in the View
                            String info = ((TextView) view).getText().toString();
                            String address = info.substring(info.length() - MAC_ADDRESS_LENGTH);
                            mBluetoothSingleton.setMacAddress(address);
                            mMacAddress = mBluetoothSingleton.getMacAddress();
                            Log.d(TAG, "MAC Address = " + mMacAddress);
                            pairDevice(unpairedItem);
                            mUnpairedArrayAdapter.remove(unpairedItem.getName() + "\n" + unpairedItem.getAddress());
                            mUnpairedDevices.remove(unpairedItem);
                            mPairedDevices.add(unpairedItem);
                            reloadListView(); //remove unpairedItem from unpaired list view
                            mUnpairedListTitle.setVisibility(View.GONE);
                            queryDevices();
                        }
                    }
                });
            }
        }
        else {
            Log.d(TAG, "Check 1");
            if (mBluetoothEnabled) {
                mDeviceNotFound.setVisibility(View.VISIBLE);
                mRetryButton.setVisibility(View.VISIBLE);
                mRetryButton.setEnabled(true);
            }
        }
    }

    public void reloadListView() {
        mUnpairedArrayAdapter.notifyDataSetChanged();
        mUnpairedList.invalidateViews();
        mUnpairedList.refreshDrawableState();
    }

    //pairs an unpaired BT device to the phone
    protected void pairDevice(BluetoothDevice device) {
        Log.d(TAG, "pairDevice before pairing method");
        try {
            Method method = device.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
            while(device.getBondState() != BluetoothDevice.BOND_BONDED) {
                Log.d(TAG, "Device bonded state = " + device.getBondState());
            }
//            mIsPaired = true;
            }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {
            mBluetoothEnabled = true;
            Log.d(TAG, "Bluetooth Enabled");
            Log.d(TAG, "requestCode = " + requestCode);
            Log.d(TAG, "resultCode = " + resultCode);
        }
        else {
            Log.d(TAG, "Bluetooth Not Enabled");
            // TODO: 16/02/16 need to add a textview to tell user they haven't enabled bluetooth and the app won't work
        }
    }

    protected void connectDevices() {
//        Log.d(TAG, "mMacAddress = " + mMacAddress);
        if (mBluetoothAdapter.getRemoteDevice(mMacAddress).equals("00:00:00:00:00:00")) {
            Log.d(TAG, "mMacAddress = " + mMacAddress);
            queryDevices();
        }
        mBluetoothSingleton.createBTSocket();
        btSocket = mBluetoothSingleton.getBtSocket();
        // Establish the connection.
        if (!mConnectionTried || !mIsDeviceOn)
            try {
                btSocket.connect();
                mIsDeviceOn = true;
                mConnectionTried = true;
                closeSocket();
                Log.d(TAG, "BT connection tried");
                queryDevices();
            } catch (IOException e) {
                mConnectionTried = true;
                queryDevices();
            }
    }

    //
    protected void startBikeM8() {
        Log.d(TAG, "How many times do you see this?");
        Intent intent = new Intent(SplashScreen.this, ViewPagerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("btSingleton", mBluetoothSingleton);
        intent.putExtras(bundle);
        startActivity(intent);
        //Close the splash screen
        finish();
    }

    private void closeSocket() {
        try {
            btSocket.close();        //If IO exception occurs attempt to close socket
            Log.d(TAG, "BT connection closed");
        } catch (IOException e2) {
            Toast.makeText(getBaseContext(), "ERROR - Could not close Bluetooth socket", Toast.LENGTH_SHORT).show();
        }
    }

    // TODO: 8/01/16 remove these calls unless they really need overriding
    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() called");
    }

    @Override
    public void onPause() {
        super.onPause();
        //Pausing can be the end of an app if the device kills it or the user doesn't open it again
        //close all connections so resources are not wasted
        Log.d(TAG, "onPause() called");
        //Close BT socket to device
        try {
            if(mIsDeviceOn)//Check this if. Might be better to be btSocket.isConnected if there is a way of instaiating btsocket before onPause is initially called
//            if(btSocket.isConnected())
            btSocket.close();
        } catch (IOException e2) {
            Toast.makeText(getBaseContext(), "ERROR - Failed to close Bluetooth socket", Toast.LENGTH_SHORT).show();
        }
    }

//    // TODO: 21/02/16 look at why this isn't used
//    //takes the UUID and creates a comms socket
//    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
//        mBluetoothSingleton.createBTSocket();
//        btSocket = mBluetoothSingleton.getBtSocket();
//        return  btSocket;
//    }

    @Override
    public void onResume() {
        super.onResume();
        if(mHasRun) queryDevices();
//        if(!mIsFound || !mIsPaired ) queryDevices();
//        else {
//            connectDevices();
//            Log.d(TAG, "connect devices called from onResume");
//        }
        Log.d(TAG, "onResume() called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
        Log.d(TAG, "onDestroy() called");
    }
    // TODO: 8/01/16 TO HERE ***********************

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_splash, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            Intent intent = new Intent(this, PrefsActivity.class);
//            startActivity(intent);          //Starts PrefsActivity from ActionBar or Menu button
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    /**
     * Discovery while connected reduces bandwidth and should not be performed unless absolutely necessary
     * Also deregister Receiver in onDestroy
     */
    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                Log.d(TAG, "Discovery Started");
                //discovery starts, we can show progress dialog or perform other tasks
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Scanning...");
                mProgressDialog.setCancelable(false);
                mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mBluetoothAdapter.cancelDiscovery();
                    }
                });
                mProgressDialog.show();
            }
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Log.d(TAG, "Discovery Finished");
                //discovery finishes, dismiss progress dialog
                mProgressDialog.dismiss();
                if (!mIsFound) {
                    mDeviceNotFound.setVisibility(View.VISIBLE);
                    mRetryButton.setVisibility(View.VISIBLE);
                    mRetryButton.setEnabled(true);
                }
                else {
                    mUnpairedListTitle.setVisibility(View.VISIBLE);
                    mUnpairedList.setVisibility(View.VISIBLE);
                    mUnpairedList.invalidateViews();
                    mUnpairedList.refreshDrawableState();
                    queryDevices();
                }
            }
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                Log.d(TAG, "Device Discovered");
                BluetoothDevice newDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (newDevice.getName().equals("BikeM8") && newDevice.getBondState() != BluetoothDevice.BOND_BONDED) {
                    mIsFound = true;
                    mIsDeviceOn = true; //do we need this
                }
                mUnpairedListTitle.setVisibility(View.GONE);
                mUnpairedList.setVisibility(View.GONE);
                mUnpairedArrayAdapter.add(newDevice.getName() + "\n" + newDevice.getAddress());
                mUnpairedDevices.add(newDevice);
                mUnpairedArrayAdapter.notifyDataSetChanged();
                Log.d(TAG, "New device added to list");
            }

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);

                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
//                    Toast.makeText(SplashScreen.this, "Paired", Toast.LENGTH_SHORT).show();
                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED){
//                    Toast.makeText(SplashScreen.this, "Unpaired", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
}
