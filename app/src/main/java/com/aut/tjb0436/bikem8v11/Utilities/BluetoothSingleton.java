package com.aut.tjb0436.bikem8v11.Utilities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Terry on 21/02/16.
 */
public class BluetoothSingleton implements Serializable {
    public static BluetoothAdapter sBluetoothAdapter;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String sMacAddress = "00:00:00:00:00:00";
    private static BluetoothDevice sDevice;
    private static BluetoothSocket sBtSocket = null;
    private static BluetoothSingleton sBtInstance = null;
    private static OutputStream sOutputStream;

    private BluetoothSingleton() {
    }

    public static BluetoothSingleton getInstance() {
        if(sBtInstance == null) sBtInstance = new BluetoothSingleton();
        return sBtInstance;
    }

    public static void createBTSocket() {
        sDevice = sBluetoothAdapter.getRemoteDevice(sMacAddress);
        try {
            sBtSocket = sDevice.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static BluetoothAdapter getBluetoothAdapter() {
        sBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return sBluetoothAdapter;
    }

    public static BluetoothSocket getBtSocket() {
        return sBtSocket;
    }

    public static void setMacAddress(String macAddress) {
        sMacAddress = macAddress;
    }

    public static String getMacAddress() {
        return sMacAddress;
    }

    public static void createOutputStream() {
        try {
            sOutputStream = sBtSocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static OutputStream getOutputStream() {
        return  sOutputStream;
    }
}
