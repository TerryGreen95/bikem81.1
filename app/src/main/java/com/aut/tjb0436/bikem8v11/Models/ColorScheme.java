package com.aut.tjb0436.bikem8v11.Models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.aut.tjb0436.bikem8v11.R;

/**
 * Created by Terry on 28/01/16.
 */
public class ColorScheme {

    protected static final String TAG = "ColorScheme";     //LogCat message TAG
    public static final String KEY_PREF_COLOR_SCHEME = "pref_color_scheme";
    private int color;
//    private final int BLUE_ON_BLACK = 0;
//    private final int GREEN_ON_BLACK = 1;
//    private final int ORANGE_ON_BLACK = 2;
    private Context mContext;

    public ColorScheme(Context context) {
        mContext = context;
        color = R.color.colorLightBlue;
    }

    public void setColor() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
//        int colorScheme = Integer.parseInt(prefs.getString(KEY_PREF_COLOR_SCHEME, "Blue on Black (Default)"));
        String colorScheme = prefs.getString(KEY_PREF_COLOR_SCHEME, "Blue on Black (Default)");
        Log.d(TAG, "colorScheme = " + colorScheme);
        switch (colorScheme) {
            case "Blue on Black (Default)": color = ContextCompat.getColor(mContext, R.color.colorLightBlue);
                break;
            case "Green On Black": color = ContextCompat.getColor(mContext, R.color.colorGreen);
                break;
            case "Orange On Black": color = ContextCompat.getColor(mContext, R.color.colorOrange);
                break;
            default: color = ContextCompat.getColor(mContext, R.color.colorLightBlue);
                break;
        }
    }

    public int getColor() {
        return color;
    }
}
