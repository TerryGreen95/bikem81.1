package com.aut.tjb0436.bikem8v11.Fragments;

import android.Manifest;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aut.tjb0436.bikem8v11.Models.ColorScheme;
import com.aut.tjb0436.bikem8v11.Models.Speed;
import com.aut.tjb0436.bikem8v11.Preferences.PrefsActivity;
import com.aut.tjb0436.bikem8v11.R;
import com.aut.tjb0436.bikem8v11.Utilities.BluetoothSingleton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created by Terry on 26/01/16.
 */
public class SpeedFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    protected static final String TAG = "SpeedFragment";     //LogCat message TAG
    private ColorScheme mColorScheme;
    private int mColor;
    private final int LIGHT_BLUE = -11608579;
    private final int GREEN = -12779776;
    private final int ORANGE = -48128;
    private View view;
    private static final String BT_SINGLETON = "btSingleton";

    /**
     * Bluetooth
     */
    private BluetoothSingleton mBluetoothSingleton;
    private BluetoothSocket btSocket;
    private OutputStream mOutputStream;

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Location Services fields
     */
    protected Location mLastLocation;
    protected Location mCurrentLocation;
    protected LocationRequest mLocationRequest;
    protected boolean mRequestingLocationUpdates = false;

    /**
     * Text fields
     */
    protected TextView mTimeDate;
    protected TextView mShowSpeed;
    protected TextView mSpeedUnits;

    /**
     * Speed fields
     */
    private Speed mSpeed;
    private int speed = 0;

    /**********************************************************************
     *********************** Lifecycle Methods ****************************
     **********************************************************************/

    /**
     * Called to initialise essential components of the fragment
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Log.d(TAG, "Created Speed Fragment");

        mBluetoothSingleton = BluetoothSingleton.getInstance();
//        mBluetoothSingleton = (BluetoothSingleton) getArguments().getSerializable(BT_SINGLETON);
////        Log.d(TAG, mBluetoothSingleton.TestString());
//        mBluetoothSingleton.createBTSocket();
//        btSocket = mBluetoothSingleton.getBtSocket();
        mBluetoothSingleton.createOutputStream();
        mOutputStream = mBluetoothSingleton.getOutputStream();
//        if(!btSocket.isConnected()) {
//            try {
//                btSocket.connect();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            try {
//                mOutputStream = btSocket.getOutputStream();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        else Log.d(TAG, "Bollox");

        mSpeed = new Speed(getActivity());              //Speed object used to get speed and speed units preferences
        mColorScheme = new ColorScheme(getActivity());  //ColorScheme object used to get colour preferences

        /**
         * Required Location Services elements
         */
        buildGoogleApiClient();                 //Builds the GoogleAPIClient below
        createLocationRequest();                //Sets LocationRequest intervals and others
        mRequestingLocationUpdates = true;      //Requests will begin with this being true
    }

    /**
     * Called to draw UI and return the layout view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);                //Make sure you have this line of code.

        view = inflater.inflate(R.layout.fragment_speed, container, false);//Inflates speed layout xml

        ConfigureTextViews();

        return view;
    }

    /**
     * onStart called when activity becomes visible to the user
     */
    @Override
    public void onStart() {
        super.onStart();
//        mGoogleApiClient.registerConnectionCallbacks(this);
        mGoogleApiClient.connect();
        ConfigureTextViews();
    }

    /**
     * onResume called when activity will start interacting with the user
     */
    @Override
    public void onResume() {
        super.onResume();
        mGoogleApiClient.registerConnectionCallbacks(this);
        ConfigureTextViews();
// TODO: 6/02/16 check this commented code thoroughly before deleting. It has all been moved into ConfigureTextView method
        /**
         * Set colour of displayed text elements
         */
//        mColorScheme.setColor();
//        mColor = mColorScheme.getColor();

//        mSpeed.setSpeedUnits(speedUnitsPref);
//        mShowSpeed.setTextColor(mColor);
//        mSpeedUnits.setTextColor(mColor);
//        mTimeDate.setTextColor(mColor);

//        mSpeedUnits.setText(String.format("%s", mSpeed.getSpeedUnits()));

//        Log.d(TAG, "Speed units now set to " + mSpeed.getSpeedUnits());
        if(mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    /**
     * onPause called when user is leaving the fragment
     */
    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
        mGoogleApiClient.unregisterConnectionCallbacks(this);
        Log.d(TAG, "Updates stopped");
    }

    /**
     * Fragment will be stopped
     */
    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        mGoogleApiClient.unregisterConnectionCallbacks(this);
    }

    /**
     * Called after onDestroyView() to do final clean up of fragment state
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.unregisterConnectionCallbacks(this);
    }

    /*********************************************************************
     *******************Google Location Services**************************
     *********************************************************************/

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Sets LocationRequest variables
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000); //need to change these to 3000 once baud is at 115200
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.

        getTimeDate();

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        /**
         * Get the last known location
         */
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mCurrentLocation == null) {
            Toast.makeText(getActivity(), R.string.no_location_detected, Toast.LENGTH_LONG).show();
        }

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    /**
     * Get the location updates using parameters set in createLocationRequest()
     */
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Update location information when location changes
     */
    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = mCurrentLocation;
        mCurrentLocation = location;
        updateUI();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
        mShowSpeed.setText("No Service");
        mSpeedUnits.setText("");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        mShowSpeed.setText("No Service");
        mSpeedUnits.setText("");
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    /**********************************************************************
     ***************************Utilities**********************************
     **********************************************************************/

    /**
     * UI view elements
     */
    protected void ConfigureTextViews() {
        mColorScheme.setColor();
        mColor = mColorScheme.getColor();
        mSpeed.setSpeedUnits();

        /**
         * Set up TextView to display current speed
         */
        mShowSpeed = (TextView) view.findViewById(R.id.show_speed);
        Typeface expansiva = Typeface.createFromAsset(getActivity().getAssets(), "fonts/expansiva_bold.ttf");
        mShowSpeed.setTypeface(expansiva);
        mShowSpeed.setTextColor(mColor);
        mShowSpeed.setGravity(Gravity.CENTER);

        /**
         * Set up TextView to display current speed units
         */
        mSpeedUnits = (TextView) view.findViewById((R.id.speed_units));
        mSpeedUnits.setTypeface(expansiva);
        mSpeedUnits.setTextColor(mColor);
        mSpeedUnits.setText(String.format("%s", mSpeed.getSpeedUnits()));

        /**
         * Set up TextView to display current time and date
         */
        mTimeDate = (TextView) view.findViewById(R.id.time_date);
        mTimeDate.setTypeface(expansiva);
        mTimeDate.setTextColor(mColor);
    }

    /**
     * Redraws UI with current information updated
     */
    private void updateUI() {
        int currentColour = currentColor();
        String dateTime = getTimeDate();
        speed = (int)mSpeed.getSpeed(mCurrentLocation, mLastLocation);
        String speedString = Integer.toString(speed);
        int units = getSpeedUnits(mSpeed.getSpeedUnits());
//        mShowSpeed.setText(String.format("%.0f", speed));
        mShowSpeed.setText(Integer.toString(speed));
        mSpeedUnits.setText(String.format("%s", mSpeed.getSpeedUnits()));

//        if(isVisibleToUser()) {

//            String data = "h" + currentColour + "*";
//            sendData(data);
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            data = "d" + dateTime + "*";
//            sendData(data);
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            data = "s" + speedString + "*";
//            sendData(data);
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            data = "k" + units + "*";
//            sendData(data);
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
    }

    public int currentColor() {
        int colour = 0;
        switch(mColor) {
            case LIGHT_BLUE: colour = 1;
                break;
            case GREEN: colour = 2;
                break;
            case ORANGE: colour = 3;
        }
        return colour;
    }

    public int getSpeedUnits(String speedUnits) {
        int units = 0;
        switch(speedUnits) {
            case "km/h": units = 1;
                break;
            case "mph": units = 2;
        }
        return units;
    }

    /**
     * Sets the current time and date and displays it at the top of the activity
     */
    public String getTimeDate() {
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
        TimeZone.getDefault();
        String dateString = sdf.format(date);
        mTimeDate.setText(dateString);//check that this till works
        return dateString;
    }

    /**************************************************************************
     *****************************Menu Options*********************************
     **************************************************************************/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        menu.clear();
        inflater.inflate(R.menu.menu_splash, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getActivity(), PrefsActivity.class);
            startActivity(intent);              //Starts PrefsActivity from ActionBar or Menu button

            return true;
        }

        return false;
    }

    // Method to send data
    private void sendData(String message) {
        byte[] msgBuffer = message.getBytes();
        try {
            //attempt to place data on the outstream to the BT device
            mOutputStream.write(msgBuffer);
            Log.d(TAG, "bytes at index 1 are " + msgBuffer[0]);
        } catch (IOException e) {
            //if the sending fails this is most likely because device is no longer there
            /**
             * start bikem8 and connect to bluetooth
             */
//            mDeviceNotFound.setVisibility(View.VISIBLE);
//            mDeviceNotFound.setText(R.string.turn_on_device);
//            mContinueButton.setVisibility(View.VISIBLE);
//            mContinueButton.setEnabled(true);
//            Toast.makeText(getBaseContext(), "ERROR - Device not found", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "sendData");
//            finish();
        }
    }
}
