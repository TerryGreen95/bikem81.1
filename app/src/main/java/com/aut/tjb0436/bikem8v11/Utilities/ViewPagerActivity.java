package com.aut.tjb0436.bikem8v11.Utilities;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.aut.tjb0436.bikem8v11.Fragments.MapsFragment;
import com.aut.tjb0436.bikem8v11.Fragments.SpeedFragment;
import com.aut.tjb0436.bikem8v11.R;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Vector;

/**
 * Created by Terry on 27/01/16.
 */
public class ViewPagerActivity extends FragmentActivity {

    protected static final String TAG = "ViewPagerActivity";     //LogCat message TAG
    public static final String BT_SINGLETON = "btSingleton";
    private List<Fragment> fragments;
    private Fragment fragment;
    private Bundle bundle;

    /**
     * Bluetooth
     */
    private BluetoothSingleton mBluetoothSingleton;
    private BluetoothSocket btSocket;
    private OutputStream mOutputStream;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        bundle = getIntent().getExtras();

        mBluetoothSingleton = (BluetoothSingleton) bundle.getSerializable(BT_SINGLETON);
//        Log.d(TAG, mBluetoothSingleton.TestString());
        mBluetoothSingleton.createBTSocket();
        btSocket = mBluetoothSingleton.getBtSocket();
        mBluetoothSingleton.createOutputStream();
        mOutputStream = mBluetoothSingleton.getOutputStream();
        if(!btSocket.isConnected()) {
            try {
                btSocket.connect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                mOutputStream = btSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else Log.d(TAG, "BTSocket is not connected");

        initialisePaging();
//        Log.d(TAG, "After initialisePaging call - onCreate");
    }

    private void initialisePaging() {
        fragments = new Vector<>();
        fragments.add(Fragment.instantiate(this, SpeedFragment.class.getName()));
        Log.d(TAG, "After fragments.add(SpeedFragment)");
        fragments.add(Fragment.instantiate(this, MapsFragment.class.getName()));
        Log.d(TAG, "After fragments.add(MapsFragment)");

        ViewPager pager = (ViewPager) findViewById(R.id.view_pager);
//        Log.d(TAG, "Check 1");
        pager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
//        Log.d(TAG, "Check 3");
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager mFragmentManager) {
            super(mFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) {
                fragment = fragments.get(position);
//                Log.d(TAG, "Check 2");
            }
            else if(position == 1) fragment = fragments.get(position);
            else fragment = fragments.get(position);

            if(bundle != null) fragment.setArguments(bundle);
            else Log.d(TAG, "Bundle is null");

            return fragment;
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }




//    @Override
//    public void onBackPressed() {
//        if (mViewPager.getCurrentItem() == 0) {
//            // If the user is currently looking at the first step, allow the system to handle the
//            // Back button. This calls finish() on this activity and pops the back stack.
//            super.onBackPressed();
//        } else {
//            // Otherwise, select the previous step.
//            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
//        }
}



