//#include <Adafruit_ST7735.h>

/***************************************************
  This is a library for the Adafruit 1.8" SPI display.

This library works with the Adafruit 1.8" TFT Breakout w/SD card
  ----> http://www.adafruit.com/products/358
The 1.8" TFT shield
  ----> https://www.adafruit.com/product/802
The 1.44" TFT breakout
  ----> https://www.adafruit.com/product/2088
as well as Adafruit raw 1.8" TFT display
  ----> http://www.adafruit.com/products/618

  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>
#include <SoftwareSerial.h>   //Software Serial Port

//This is for the Arduino/seeeduino Mega.
//Not all pins on the Mega and Mega 2560 support change interrupts, 
//so only the following can be used for RX: 10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69
//#define RxD 2 
//#define TxD 3

#define RxD 0 
#define TxD 1

#define DEBUG_ENABLED  1
 
//SoftwareSerial blueToothSerial(RxD,TxD);//sets the pins for the SoftwareSerial port that 
                                        //allows pins other than 0 and 1 to used for serial comms

// For the breakout, you can use any 2 or 3 pins
// These pins will also work for the 1.8" TFT shield
#define TFT_CS     10
#define TFT_RST    9  // you can also connect this to the Arduino reset
                      // in which case, set this #define pin to 0!
#define TFT_DC     8

// For TFT Screen:
// Option 1 (recommended): must use the hardware SPI pins
// (for UNO thats sclk = 13 and sid = 11) and pin 10 must be
// an output. This is much faster - also required if you want
// to use the microSD card (see the image drawing example)
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

// Option 2: use any pins but a little slower!
#define TFT_SCLK 13  // set these to be whatever pins you like!
#define TFT_MOSI 11   // set these to be whatever pins you like!
//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

//set up pin for LED output
int LED = 12;

// Setup for comms from Android device
#define TIME_TEXT_SIZE 17
#define SPEED_TEXT_SIZE 3

char colour = 1;
char timeBuffer[TIME_TEXT_SIZE] = {};
String timeText = "";
char speedText[SPEED_TEXT_SIZE];
char speedUnits = 1;
String speedUnitsText;
byte imageArray[64];

// run once to setup environment
void setup(void) {
//  Serial.begin(38400);//set serial comms baud rate
Serial.begin(57600);
  pinMode(LED, OUTPUT);//set LED pin to output
  digitalWrite(LED, HIGH);//turn on LED
  delay(2000);//wait 2 seconds
  pinMode(RxD, INPUT);//set receiver pin to input 
  pinMode(TxD, OUTPUT);//set transmit pin to output

  // This is my screen:
  // Use this initializer if you're using a 1.8" TFT
  tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab

  // Use this initializer (uncomment) if you're using a 1.44" TFT
  //tft.initR(INITR_144GREENTAB);   // initialize a ST7735S chip, black tab

//  Serial.println("Initialized");
  tft.fillScreen(ST7735_BLACK);
  tft.setRotation(1); //rotates TFT image to portrait view
  tft.setTextColor(ST7735_WHITE);
  tft.print("ST7735 TFT Test");//display this on TFT screen remove later
  setupBlueToothConnection();//initialise bluetooth settings
}

void setupBlueToothConnection()
{
//  blueToothSerial.begin(57600); //Set BluetoothBee BaudRate to default baud rate 57600
////  clearSoftwareSerial();
//  blueToothSerial.print("\r\n+STWMOD=0\r\n"); //set the bluetooth work in slave mode
//  blueToothSerial.print("\r\n+STNA=BikeM8\r\n"); //set the bluetooth name as "BikeM8"
//  blueToothSerial.print("\r\n+STOAUT=1\r\n"); // Permit Paired device to connect me
//  blueToothSerial.print("\r\n+STAUTO=0\r\n"); // Auto-connection should be forbidden here
//  delay(2000); // This delay is required.
//  blueToothSerial.print("\r\n+INQ=1\r\n"); //make the slave bluetooth inquirable 
  
  Serial.print("\r\n+STWMOD=0\r\n"); //set the bluetooth work in slave mode
  Serial.print("\r\n+STNA=BikeM8\r\n"); //set the bluetooth name as "BikeM8"
  Serial.print("\r\n+STOAUT=1\r\n"); // Permit Paired device to connect me
  Serial.print("\r\n+STAUTO=0\r\n"); // Auto-connection should be forbidden here
  delay(2000); // This delay is required.
  Serial.print("\r\n+INQ=1\r\n"); //make the slave bluetooth inquirable
  
  tft.setCursor(0, 0);
  tft.fillScreen(ST7735_BLACK);
  tft.print("BT is incquirable!");
  delay(2000); // This delay is required.
  digitalWrite(LED, LOW);
  tft.fillScreen(ST7735_BLACK);
}

// run continuously
void loop() {
//  if(blueToothSerial.isListening()) {
//    while(blueToothSerial.available() > 0) { //check if there's any data sent from the remote bluetooth shield

//if(Serial) {
    while(Serial.available() > 0) {
      memset(imageArray, 0, sizeof(imageArray));
      Serial.flush();//wait for outgoing data to be transmitted so Serial UART port ready to receive fresh data
//      Serial.write(blueToothSerial.read());//write data received at SoftwareSerial to Serial port
//      delay(200);//Check how long this needs
      int index = 0;
      boolean dataIsIn = false;
      boolean imageIsIn = false;
      timeText = "";
//      while(!dataIsIn) {
//        char data = blueToothSerial.read();
//        delay(1);
//        if(data == 'h') {
//          data = blueToothSerial.read();
//          delay(10);
//          while(data != '*') {
//            colour = data;
//            data = blueToothSerial.read();
//            delay(1);
//          }
//        }
//        else if(data == 'd') {
//          data = blueToothSerial.read();
//          delay(1);
//          index = 0;
//          while(data != '*') {
//            timeBuffer[index++] = data;
//            data = blueToothSerial.read();
//            delay(1);
//          }
//          for(int i = 0; i < TIME_TEXT_SIZE; i++) {
//            timeText += timeBuffer[i];
//          }
//        }
//        else if(data == 's') {
//          data = blueToothSerial.read();
//          delay(1);
//          index = 0;
//          while(data != '*') {
//            speedText[index++] = data;
//            data = blueToothSerial.read();
//            delay(1);
//          }
//        }
//        else if(data == 'k') {
//          data = blueToothSerial.read();
//          delay(1);
//          while(data != '*') {
//            speedUnits = data;
//            data = blueToothSerial.read();
//            delay(1);
//          }
//          speedUnitsText = getSpeedUnits(speedUnits);
//          dataIsIn = true;
//        }
//      }
      while(!imageIsIn) {
//        byte data = blueToothSerial.read();

        int sizeOfArray = 0;
            index = 0;
        tft.setCursor(0, 0);
        byte data = Serial.read();

        if(data == 'm') {
          
          tft.print("image has started coming\n");
//          delay(250);
//          tft.fillScreen(ST7735_BLACK);
//          data = blueToothSerial.read();

//          data = Serial.read();

          delay(1);
//          while(data != '*') {
            Serial.readBytesUntil('*',imageArray, 500);
//            imageArray[index++] = data;
//            tft.print(" ");
//            tft.print(data);
            //            delay(1);
//            data = blueToothSerial.read();

//              data = Serial.read();
//              delay(1);
              
//            if(data == '*') {
              tft.print("image has finished\n");
              imageIsIn = true;
//              data = 'q';
            }
            delay(1);
          }
//        }
//      }
//      tft.setCursor(0, 0);
      tft.setTextColor(getColor(colour));
      tft.setTextSize(1);
//      tft.fillScreen(ST7735_BLACK);
      if(imageIsIn) {
        tft.print("image has arrived.\n");
        for(int i = 0; i < sizeof(imageArray); i++) {
          tft.print(imageArray[i]);
          tft.print(" ");
        }
        tft.setTextColor(ST7735_WHITE);
        memset(imageArray, 0, sizeof(imageArray));
//        imageIsIn = false;
      }
//      tft.print(timeText);
//      if(!imageIsIn) {
//      tft.setCursor(30, 50);
//      tft.setTextSize(3);
//      }
//      else {
//        tft.setCursor(0, 10);
//        tft.setTextSize(2);
//      }
//      tft.print(speedText);
//      tft.print(" ");
//      tft.print(speedUnitsText);
//      tft.setCursor(0, 0);
//      tft.setTextSize(1);
    }
//  }
}

uint16_t getColor(char colour) {
  uint16_t color;
  if(colour == '1') color = ST7735_CYAN;
  else if(colour == '2') color = ST7735_GREEN;
  else if(colour == '3') color = ST7735_RED;
  return color;
}
 
String getSpeedUnits(char units) {
  if(units == '1') return "km/h";
  else if(units == '2') return "mph";
}

//void function1()
//{
//  //Enter what you want the command to initiate (e.g LED, Servo etc)
//  digitalWrite(LED, HIGH);
//  tft.setCursor(0, 0);
//  tft.fillScreen(ST7735_BLACK);
//  tft.setTextColor(ST7735_WHITE);
//  tft.print("Function 1");
//}
// 
//void function2()
//{
//  //Enter what you want the command to initiate (e.g LED, Servo etc)
//  digitalWrite(LED, LOW);
//  tft.setCursor(0, 0);
//  tft.fillScreen(ST7735_BLACK);
//  tft.setTextColor(ST7735_WHITE);
//  tft.print("Function 2");
//}
//
//void text(char *text, uint16_t color) {
//  tft.fillScreen(ST7735_BLACK);
//  tft.setCursor(0, 0);
//  tft.setTextColor(color);
//  tft.setTextWrap(true);
//  tft.print(text);
//}



